"use strict";



var initPhotoSwipeFromDOM = function (gallerySelector) {

    // parse slide data (url, title, size ...) from DOM elements
    // (children of gallerySelector)
    var parseThumbnailElements = function (el) {
        //        var thumbElements = el.childNodes,
        var thumbElements = el.querySelectorAll('figure'),
            numNodes = thumbElements.length,
            items = [],
            figureEl,
            linkEl,
            size,
            item;

        for (var i = 0; i < numNodes; i++) {

            figureEl = thumbElements[i]; // <figure> element

            // include only element nodes
            if (figureEl.nodeType !== 1) {
                continue;
            }

            linkEl = figureEl.children[0]; // <a> element

            size = linkEl.getAttribute('data-size').split('x');

            // create slide object
            item = {
                src: linkEl.getAttribute('href'),
                w: parseInt(size[0], 10),
                h: parseInt(size[1], 10)
            };



            if (figureEl.children.length > 1) {
                // <figcaption> content
                item.title = figureEl.children[1].innerHTML;
            }

            if (linkEl.children.length > 0) {
                // <img> thumbnail element, retrieving thumbnail url
                item.msrc = linkEl.children[0].getAttribute('src');
            }

            item.el = figureEl; // save link to element for getThumbBoundsFn
            items.push(item);
        }

        return items;
    };

    // find nearest parent element
    var closest = function closest(el, fn) {
        return el && (fn(el) ? el : closest(el.parentNode, fn));
    };

    // triggers when user clicks on thumbnail
    var onThumbnailsClick = function (e) {
        e = e || window.event;
        //        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        // find root element of slide
        var clickedListItem = closest(eTarget, function (el) {
            return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
        });

        if (!clickedListItem) {
            return;
        }

        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        // find index of clicked item by looping through all child nodes
        // alternatively, you may define index via data- attribute
        var clickedGallery = clickedListItem.parentNode,
            childNodes = clickedListItem.parentNode.childNodes,
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index;

        for (var i = 0; i < numChildNodes; i++) {
            if (childNodes[i].nodeType !== 1) {
                continue;
            }

            if (childNodes[i] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }



        if (index >= 0) {
            // open PhotoSwipe if valid index found
            openPhotoSwipe(index, clickedGallery);
        }
        return false;
    };

    // parse picture index and gallery index from URL (#&pid=1&gid=2)
    var photoswipeParseHash = function () {
        var hash = window.location.hash.substring(1),
            params = {};

        if (hash.length < 5) {
            return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
            if (!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');
            if (pair.length < 2) {
                continue;
            }
            params[pair[0]] = pair[1];
        }

        if (params.gid) {
            params.gid = parseInt(params.gid, 10);
        }

        return params;
    };

    var openPhotoSwipe = function (index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
            gallery,
            options,
            items;

        items = parseThumbnailElements(galleryElement);

        // define options (if needed)
        options = {

            // define gallery index (for URL)
            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

            getThumbBoundsFn: function (index) {
                // See Options -> getThumbBoundsFn section of documentation for more info
                var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect();

                return {
                    x: rect.left,
                    y: rect.top + pageYScroll,
                    w: rect.width
                };
            }

        };

        // PhotoSwipe opened from URL
        if (fromURL) {
            if (options.galleryPIDs) {
                // parse real index when custom PIDs are used
                // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                for (var j = 0; j < items.length; j++) {
                    if (items[j].pid == index) {
                        options.index = j;
                        break;
                    }
                }
            } else {
                // in URL indexes start from 1
                options.index = parseInt(index, 10) - 1;
            }
        } else {
            options.index = parseInt(index, 10);
        }

        // exit if index not found
        if (isNaN(options.index)) {
            return;
        }

        if (disableAnimation) {
            options.showAnimationDuration = 0;
        }

        // Pass data to PhotoSwipe and initialize it
        gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    };

    // loop through all gallery elements and bind events
    var galleryElements = document.querySelectorAll(gallerySelector);

    for (var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i + 1);
        galleryElements[i].onclick = onThumbnailsClick;
    }

    // Parse URL and open gallery if it contains #&pid=3&gid=1
    var hashData = photoswipeParseHash();
    if (hashData.pid && hashData.gid) {
        openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
    }
};


var SystemCommand = (function () {
    var arrMonths = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"];

    //tmim для ie8
    if (typeof String.prototype.trim !== 'function') {
        String.prototype.trim = function () {
            return this.replace(/^\s+|\s+$/g, '');
        };
    }

    return {
        getCustomDate: function (oDate) {
            var sResult = oDate.Day + " " + arrMonths[oDate.Month - 1] + " " + oDate.Year;
            return sResult;
        },
        getGETRequestParameters: function () {
            var tmp = new Array(); // два вспомагательных
            var tmp2 = new Array(); // массива
            var param = {}; //new Array();
            var get = location.search; // строка GET запроса
            if (get != '') {
                tmp = (get.substr(1)).split('&'); // разделяем переменные
                for (var i = 0; i < tmp.length; i++) {
                    tmp2 = tmp[i].split('='); // массив param будет содержать
                    param[tmp2[0]] = tmp2[1]; // пары ключ(имя переменной)->значение
                }
                return param;
            }
            return 'none';
        },

        getHASHRequestParameters: function () {
            var tmp = new Array(); // два вспомагательных
            var tmp2 = new Array(); // массива
            var param = {}; //new Array();
            var hash = location.hash; // строка HASH параметров
            if (hash != '') {
                tmp = (hash.substr(1)).split('&'); // разделяем переменные
                for (var i = 0; i < tmp.length; i++) {
                    tmp2 = tmp[i].split('='); // массив param будет содержать
                    param[tmp2[0]] = tmp2[1]; // пары ключ(имя переменной)->значение
                }
                return param;
            }
            return 'none';
        },

        getJSON: function (url, callback) {
            var responce;
            var xhr = new XMLHttpRequest();
            //        xhr.open('GET', url, isAsync);
            xhr.open('GET', url, true);
            xhr.send();
            xhr.onreadystatechange = function () {
                if (this.readyState != 4) return;

                if (xhr.status != 200) {
                    // обработать ошибку
                } else {
                    responce = JSON.parse(xhr.responseText)
                    if (typeof (callback) == 'function') {
                        callback(responce);
                    }
                }
            }
        },

        addClass: function (obj, sClass) {
            if (obj.classList) {
                obj.classList.add(sClass);
            } else {
                if (obj.className.indexOf(sClass) == -1) {
                    obj.className = obj.className + ' ' + sClass;
                }
            }
        },

        removeClass: function (obj, sClass) {
            if (obj.classList) {
                obj.classList.remove(sClass);
            } else {
                obj.className = obj.className.replace(sClass, '').trim();
            }

        },

        switchClass: function (obj, sClass) {
            if (obj.classList) {
                if (obj.classList.contains(sClass)) {
                    obj.classList.remove(sClass);
                } else {
                    obj.classList.add(sClass);
                }
            } else {
                if (obj.className.indexOf(sClass) == -1) {
                    obj.className = obj.className + ' ' + sClass;
                } else {
                    obj.className = obj.className.replace(sClass, '').trim();
                }
            }
        }
    }
}());

var Page = (function () {
    return {
        initAnchors: function () { //инициация якорей
            var anchors = document.querySelectorAll('a[href^="#"]');
            for (var i = 0; i < anchors.length; i++) {
                if (anchors[i].addEventListener) {
                    anchors[i].addEventListener('click', EventListener);
                } else {
                    anchors[i].attachEvent('onclick', EventListener);
                }
            }

            function EventListener(e) {
                var curTarget = e.currentTarget || e.srcElement;
                if (typeof (e.preventDefault) == 'function') {
                    e.preventDefault();
                };
                setTimeout(function () {
                    Animation.SmoothScroll(curTarget.hash, 500);
                }, 200);
                return false;
            }
        },
        initFixedMenu: function (sId) { //приклеивание меню
            Event.add(window, 'scroll', EventListener);

            function EventListener(e) {
                var oPreviousElement;
                var iMenuOffset;
                var pageYScroll = window.pageYOffset || document.documentElement.scrollTop;
                var oMenu = document.getElementById(sId);
                if (oMenu) {
                    oPreviousElement = (oMenu.previousElementSibling) ? oMenu.previousElementSibling : oMenu.previousSibling;
                    iMenuOffset = oPreviousElement.offsetHeight + oPreviousElement.offsetTop;
                    if (iMenuOffset <= pageYScroll) {
                        oPreviousElement.style['margin-bottom'] = (window.getComputedStyle) ? window.getComputedStyle(oMenu).height : oMenu.currentStyle.height;
                        SystemCommand.addClass(oMenu, 'fixed');
                    } else {
                        SystemCommand.removeClass(oMenu, 'fixed');
                        oPreviousElement.style['margin-bottom'] = '';
                    }
                }
            }
        },
        switchMenuView: function (sId, iWidth) {
            EventListener();
            Event.add(window, 'resize', EventListener);

            function EventListener(e) {
                var oMenu = document.getElementById(sId);
                var innerWidth = window.innerWidth || document.body.clientWidth;
                if (innerWidth < iWidth) {
                    SystemCommand.addClass(oMenu, 'hidden');
                    Event.add(oMenu, 'click', LittleMenuClick);
                } else {
                    SystemCommand.removeClass(oMenu, 'hidden');
                    Event.remove(oMenu, 'click', LittleMenuClick);
                }

                function LittleMenuClick(e) {
                    SystemCommand.switchClass(this, 'hidden');
                    //                Event.remove(this, 'click', LittleMenuClick);
                }
            }
        }
    }
}());

var Gallery = (function () {
    var oSingleGallery; //конкретная галерея
    var oAllGalleries; //титульники всех галерей
    var iCurPage; //последняя используемая страница в конкретной категории
    var toLoad = false; //загружать ли галерею

    //    this.GetSingleGalleryObj = function () {
    //        return oSingleGallery;
    //    }
    //
    //    this.GetJSONSingleGalleryObj = function () {
    //        return JSON.stringify(oSingleGallery);
    //    }
    //
    //    this.GetAllGalleriesObj = function () {
    //        return oAllGalleries;
    //    }

    return {
        LoadGallery: function (callback) {
            SystemCommand.getJSON('json/pictureslists.json', function (data) {
                var category = (SystemCommand.getGETRequestParameters())['category'] || 'none';
                if ((data[category] != undefined) && (data[category] != null)) {
                    SystemCommand.getJSON(data[category]['ListPath'], function (data) {
                        oSingleGallery = data;
                        toLoad = true;
                        if (typeof (callback) == 'function') {
                            callback();
                        }
                    });
                    return false;
                }
            });
        },

        LoadCategory: function (callback) {
            SystemCommand.getJSON('json/pictureslists.json', function (data) {
                oAllGalleries = data;
                if (typeof (callback) == 'function') {
                    callback();
                }
            });
        },

        EnterNewPhotos: function (n) {

            for (var i = oSingleGallery.Pictures.length + 1; i <= n; i++) {
                oSingleGallery.Pictures.push({
                    "CreatedDate": {
                        "Day": 24,
                        "Month": 2,
                        "Year": 2015
                    },
                    "Description": "",
                    "FileName": i + ".jpg",
                    "PictureName": ""
                });
            }
        },

        //получить размеры картинок
        EnterWHtoPictures: function () {
            var url, oSinglePhoto, i;
            var GalleryLength = oSingleGallery['Pictures'].length;


            var onloadImage = function () {
                oSingleGallery['Pictures'][this.i]['Width'] = this.naturalWidth;
                oSingleGallery['Pictures'][this.i]['Height'] = this.naturalHeight;
                console.log(this.i, this.src, this.naturalWidth, this.naturalHeight);
            };

            for (i = 0; i < GalleryLength; i++) {
                var img = new Image();
                img.onload = onloadImage;
                oSinglePhoto = oSingleGallery['Pictures'][i];
                url = oSingleGallery['RootFolder'] + "/" + oSingleGallery['BigPictureFolder'] + "/" + oSinglePhoto['FileName'];
                img.src = url;
                img['i'] = i;
            }
        },

        GetJSONSingleGalleryObj: function () {
            return JSON.stringify(oSingleGallery);
        },

        IncPageCount: function () {
            function GetText(AInputText, AReplaceText) {
                var VRegExp1 = new RegExp(/\Wpage=\d{0,}/g);
                var VRegExp2 = new RegExp(/page=\d{0,}/g);
                var VResult;
                if (VRegExp1.test(AInputText)) {
                    VResult = AInputText.replace(VRegExp2, AReplaceText);
                } else {
                    VResult = AInputText;
                }
                return VResult;
            }

            //        window.location.hash = GetText(window.location.hash, 'page=' + (iCurPage - 0 + 1));
            //        window.location.hash = GetText(window.location.hash, 'page=' + (iCurPage - 0 + 1));
            window.location.replace(GetText(window.location.hash, 'page=' + (iCurPage - 0 + 1)));
        },

        AddCategoryTitle: function (sContainer) {
            var oContainer = document.getElementById(sContainer);
            if ((!oContainer) || (!oSingleGallery)) {
                return;
            }
            oContainer.innerHTML = oSingleGallery['Title'];
        },

        GenerateDOMObjectOneCategory: function (sContainer) {
            var i, iCounter, iStart; //счетчик, кол-во размещаемых фото, с какого номера начать
            var iNum = 20; //по сколько размещать
            var page = SystemCommand.getHASHRequestParameters()['page'] - 0;
            var oPicturesList = oSingleGallery['Pictures'];
            var oMoreLink = document.getElementById('MoreLink');
            var oContainer = document.getElementById(sContainer);
            var oNextElement; // для добавления кнопки "еще"
            var GalleryTable = '';
            //        jQuery(".page-title").append(" / " + oList.Title);

            if (oPicturesList.length > 0) {
                if (!page) {
                    page = 1;
                    window.location.replace('#page=1');
                }

                if (page == iCurPage) {
                    return;
                }

                if (page < iCurPage) {
                    toLoad = true;
                }
                iCurPage = page;

                if (toLoad) {
                    iCounter = (oPicturesList.length > iNum * page) ? iNum * page : oPicturesList.length;
                    iStart = oPicturesList.length - 1;

                } else {
                    iCounter = (oPicturesList.length > iNum * page) ? iNum : oPicturesList.length % iNum;
                    iStart = oPicturesList.length - (iNum * (page - 1)) - 1;
                }

                //Нельзя стартовать с отрицательной позиции
                if (iStart <= 0) {
                    return;
                }

                //            for (i = 0; i < oPicturesList.length; i++) {
                for (i = 0; i < iCounter; i++) {

                    var oSinglePhoto = oPicturesList[iStart - i];

                    //добавление подписи новых фотографий
                    //                var sNewLable = "";
                    //                var oDate = oSinglePhoto.CreatedDate;
                    //                var dDate = new Date(oDate.Year, oDate.Month - 1, oDate.Day);
                    //                if ((new Date(dDate.getTime() + (5 * 86400000))) > (new Date())) {
                    //                    sNewLable = '<div class="foto-is-new">new</div>';
                    //                }

                    GalleryTable = GalleryTable +
                        '<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">' +
                        '<a href="' + oSingleGallery.RootFolder + "/" + oSingleGallery.BigPictureFolder + "/" + oSinglePhoto.FileName + '" itemprop="contentUrl" data-size="' + oSinglePhoto.Width + 'x' + oSinglePhoto.Height + '">' +
                        '<img class="middle" src="' + oSingleGallery.RootFolder + "/" + oSingleGallery.LittlePictureFolder + "/" + oSinglePhoto.FileName + '" itemprop="thumbnail" alt="Image description" onerror="this.src = \'img/no-photo.png\'"/>' +
                        '</a>' +
                        //                    '<figcaption itemprop="caption description">Image caption 1</figcaption>' +
                        '</figure>';
                }

                if (!oMoreLink) {
                    oMoreLink = document.createElement('a');
                    oMoreLink.setAttribute('id', 'MoreLink');
                    oMoreLink.setAttribute('class', 'more-link');
                    oMoreLink.setAttribute('href', 'javascript:Gallery.IncPageCount();Gallery.GenerateDOMObjectOneCategory(\'FotoArea\')');

                }

                if (oMoreLink.remove) {
                    oMoreLink.remove(); //удаляем старую ссылку
                }
                if (oMoreLink.removeNode) {
                    oMoreLink.innerHTML = '';
                    oMoreLink.removeNode(); //удаляем старую ссылку
                }

                if (toLoad) {
                    oContainer.innerHTML = GalleryTable;
                    toLoad = false;
                } else {
                    oContainer.innerHTML = oContainer.innerHTML + GalleryTable;
                }

                if (oPicturesList.length > iNum * page) {
                    oNextElement = (oContainer.nextElementSibling) ? oContainer.nextElementSibling : oContainer.nextSibling;
                    oContainer.parentElement.insertBefore(oMoreLink, oNextElement);
                    oMoreLink.innerHTML = 'еще';
                }
            } else {
                oContainer.innerHTML = '<span>В категории нет фото</span>';
            }
        },

        GenerateDOMObjectAllCategories: function (sContainer) {

            var oContainer = document.getElementById('Categories');
            if ((oAllGalleries != null) && (oAllGalleries != undefined)) {
                var GalleryTable = '';
                //            var i = 0; //счетчик
                //            var PathName = oAllGalleries.GlobalPathName;

                for (var key in oAllGalleries) {

                    var val = oAllGalleries[key];
                    GalleryTable = GalleryTable +
                        '<div class="category-item">' +
                        '<a href="gallery.html?category=' + key + '" title="' + val.Title + '">' +
                        '<img class="middle" src="' + val.LableImgLink + '" alt="" onerror="this.src = \'img/no-photo.png\'"/>' +

                        '<div class="category-name"><div class="table"><div class="table-cell"><span>' + val.Title + '</span></div>' +
                        '</div>' +
                        '</div>' +
                        '</a>' +


                        '</div> ';
                }


                oContainer.innerHTML = GalleryTable;
            } else {
                oContainer.innerHTML = '<span>Нет фото</span>';
            }
        }
    }


}());

//модуль анимации
var Animation = (function () {

    function delta(progress) {
        return 1 - Math.sin((1 - progress) * Math.PI / 2);
    }

    return {
        SmoothScroll: function (id, dur) { //плавный скролл
            var element = document.querySelector(id);
            var from = window.pageYOffset || document.documentElement.scrollTop;
            //            console.log(element.offsetTop,document.querySelector('[fixed]').offsetHeight);
            var to = (document.querySelector('[fixed]')) ? element.offsetTop - document.querySelector('[fixed]').offsetHeight : element.offsetTop;
            var duration = dur || 1000;
            var start = new Date().getTime();

            setTimeout(function smoothEdit() {
                var now = (new Date().getTime()) - start;
                var progress = now / duration;
                //            console.log(progress);
                if (progress > 1)
                    var result = to;
                else
                    var result = (to - from) * delta(progress) + from;

                window.scrollTo(0, result);

                if (progress < 1)
                    setTimeout(smoothEdit, 10);
            }, 10);
        }
    }

}());

//модуль кроссбраузерного добавления обработчика
Event = (function () {

    var guid = 0

    function fixEvent(event) {
        event = event || window.event

        if (event.isFixed) {
            return event
        }
        event.isFixed = true

        event.preventDefault = event.preventDefault || function () {
            this.returnValue = false
        }
        event.stopPropagation = event.stopPropagaton || function () {
            this.cancelBubble = true
        }

        if (!event.target) {
            event.target = event.srcElement
        }

        if (!event.relatedTarget && event.fromElement) {
            event.relatedTarget = event.fromElement == event.target ? event.toElement : event.fromElement;
        }

        if (event.pageX == null && event.clientX != null) {
            var html = document.documentElement,
                body = document.body;
            event.pageX = event.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0);
            event.pageY = event.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0);
        }

        if (!event.which && event.button) {
            event.which = (event.button & 1 ? 1 : (event.button & 2 ? 3 : (event.button & 4 ? 2 : 0)));
        }

        return event
    }

    /* Вызывается в контексте элемента всегда this = element */

    function commonHandle(event) {
        event = fixEvent(event)

        var handlers = this.events[event.type]

        for (var g in handlers) {
            var handler = handlers[g]

            var ret = handler.call(this, event)
            if (ret === false) {
                event.preventDefault()
                event.stopPropagation()
            }
        }
    }

    return {
        add: function (elem, type, handler) {

            if (elem.setInterval && (elem != window && !elem.frameElement)) {
                elem = window;
            }



            if (!elem.events) {
                elem.events = {}
                elem.handle = function (event) {
                    if (typeof Event !== "undefined") {
                        return commonHandle.call(elem, event)
                    }
                }
            }


            if (!elem.events[type]) {
                elem.events[type] = {}

                if (elem.addEventListener)
                    elem.addEventListener(type, elem.handle, false)
                else if (elem.attachEvent)
                    elem.attachEvent("on" + type, elem.handle)
            } else {
                //проверка на наличие именованной функции

                for (var key in elem.events[type]) {
                    if ((handler.name != '') && elem.events[type][key]['name'] == handler.name) {
                        return;
                    }
                }
            }



            if (!handler.guid) {
                handler.guid = ++guid
            }

            elem.events[type][handler.guid] = handler

        },

        remove: function (elem, type, handler) {
            var handlers = elem.events && elem.events[type]

            if (!handlers) return

            delete handlers[handler.guid]

            for (var any in handlers) return
            if (elem.removeEventListener)
                elem.removeEventListener(type, elem.handle, false)
            else if (elem.detachEvent)
                elem.detachEvent("on" + type, elem.handle)

            delete elem.events[type]


            for (var any in elem.events) return
            try {
                delete elem.handle
                delete elem.events
            } catch (e) { // IE
                elem.removeAttribute("handle")
                elem.removeAttribute("events")
            }
        }
    }
}());
